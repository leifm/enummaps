{-# LANGUAGE DerivingVia #-}

module Data.EnumOrd (EnumOrd (..)) where

import Data.Function (on)
import Data.Ord (comparing)

newtype EnumOrd a = EnumOrd {unEnumOrd :: a}
  deriving (Show, Read) via a

instance Enum a => Eq (EnumOrd a) where
  (==) = (==) `on` (fromEnum . unEnumOrd)

instance Enum a => Ord (EnumOrd a) where
  compare = comparing $ fromEnum . unEnumOrd
